import { Migration } from '@mikro-orm/migrations';

export class Migration20220519212045 extends Migration {

  async up(): Promise<void> {
    this.addSql('create table "user" ("id" serial primary key, "created_at" timestamptz(0) not null default now(), "updated_at" timestamptz(0) null, "deleted_at" timestamptz(0) null, "username" varchar(50) not null, "password" varchar(255) not null);');
    this.addSql('alter table "user" add constraint "user_username_unique" unique ("username");');

    this.addSql('create table "user_likes" ("id" serial primary key, "user_liked_id" int not null, "user_likes_id" int not null);');

    this.addSql('alter table "user_likes" add constraint "user_likes_user_liked_id_foreign" foreign key ("user_liked_id") references "user" ("id") on update cascade on delete cascade;');
    this.addSql('alter table "user_likes" add constraint "user_likes_user_likes_id_foreign" foreign key ("user_likes_id") references "user" ("id") on update cascade on delete cascade;');
  }

  async down(): Promise<void> {
    this.addSql('alter table "user_likes" drop constraint "user_likes_user_liked_id_foreign";');

    this.addSql('alter table "user_likes" drop constraint "user_likes_user_likes_id_foreign";');

    this.addSql('drop table if exists "user" cascade;');

    this.addSql('drop table if exists "user_likes" cascade;');
  }

}
