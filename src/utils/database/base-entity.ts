import {
  Filter, OptionalProps, PrimaryKey, Property,
} from "@mikro-orm/core";

@Filter({
  name: "excludeDeleted",
  cond: {
    deletedAt: null,
  },
  default: true,
})
export default abstract class BaseEntity {
  @PrimaryKey()
    id!: number;

  @Property({ defaultRaw: `now()`, onCreate: () => new Date(), hidden: true })
    createdAt!: Date;

  @Property({
    onCreate: () => null, onUpdate: () => new Date(), nullable: true, hidden: true,
  })
    updatedAt?: Date;

  @Property({ onCreate: () => null, nullable: true, hidden: true })
    deletedAt?: Date;

  [OptionalProps]? = "createdAt";
}
