import { Request, Response, NextFunction } from "express";
import {
  DriverException as MikroORMException,
  ValidationError as MikroORMValidationError,
  UniqueConstraintViolationException,
} from "@mikro-orm/core";
import { ConflictException, HttpException, InternalServerException } from "./exceptions/exceptions";
import { BaseException } from "./exceptions/base-exception";

export const errorHandler = (err: Error, req: Request, res: Response, next: NextFunction) => {
  if (res.headersSent) {
    return next(err);
  }

  console.error(err);

  if (err instanceof MikroORMException || err instanceof MikroORMValidationError) {
    let error: HttpException;

    switch (true) {
      case err instanceof UniqueConstraintViolationException:
        error = new ConflictException("This entity already exists.");
        break;
      default:
        error = new InternalServerException();
        break;
    }

    error = new HttpException(error.name, error.status, error.message);

    return res.status(error.status).json(error).end();
  }
  let error: HttpException;
  if (err instanceof BaseException) {
    const message = err.message?.replace(/\n/g, "").trim();
    error = new HttpException(err.name, err.status, message);
  } else {
    error = new HttpException();
  }

  return res.status(error.status).json(error).end();
};
