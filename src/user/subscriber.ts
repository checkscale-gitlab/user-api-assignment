import { ChangeSetType, EventSubscriber, FlushEventArgs } from "@mikro-orm/core";
import bcrypt from "bcrypt";
import User from "./entity";

export default class UserSubscriber implements EventSubscriber {
  async onFlush(args: FlushEventArgs): Promise<void> {
    const changeSets = args.uow.getChangeSets();
    const users = changeSets.filter((cs) => cs.entity instanceof User);

    const cs = users.find((user) => user.type === ChangeSetType.CREATE
      || (user.type === ChangeSetType.UPDATE
        && user.entity.password !== user.originalEntity?.password));

    if (cs) {
      const salt = await bcrypt.genSalt(10);
      cs.entity.password = await bcrypt.hash(cs.entity.password, salt);
      args.uow.recomputeSingleChangeSet(cs.entity);
    }
  }
}
