import jwt from "jsonwebtoken";
import User from "../entity";
import { IUserRO } from "./interfaces";

const generateJWT = (user: User) => {
  const SECRET = process.env.SECRET || "";

  const today = new Date();
  const exp = new Date(today);
  exp.setDate(today.getDate() + 60);

  return jwt.sign({
    username: user.username,
    exp: exp.getTime() / 1000,
    id: user.id,
  }, SECRET);
};

export const buildUserRO = (user: User): IUserRO => {
  const userRO = {
    username: user.username,
    token: generateJWT(user),
  };

  return { user: userRO };
};
