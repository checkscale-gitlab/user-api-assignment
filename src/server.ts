import "reflect-metadata";
import http from "http";
import express from "express";
import cors from "cors";
import {
  EntityManager, IMigrator, ISeedManager, MikroORM, RequestContext,
} from "@mikro-orm/core";
import { PostgreSqlDriver } from "@mikro-orm/postgresql";
import dotenv from "dotenv";
import userRouter from "./user/route";
import User from "./user/entity";
import { errorHandler } from "./utils/error-handler";
import UserRepository from "./user/repository";

declare global {
  namespace Express {
    interface Request {
      user: User
    }
  }
}

export const DI = {} as {
  server: http.Server;
  orm: MikroORM<PostgreSqlDriver>,
  em: EntityManager,
  migrator: IMigrator,
  seeder: ISeedManager,
  userRepository: UserRepository;
};

dotenv.config();
export const app = express();
const port = process.env.PORT || 3000;

export const init = (async () => {
  console.log("is this working2");
  DI.orm = await MikroORM.init();
  DI.em = DI.orm.em;
  DI.migrator = DI.orm.getMigrator();
  DI.seeder = DI.orm.getSeeder();

  DI.userRepository = DI.orm.em.getRepository(User);

  app.use(express.json());

  await DI.migrator.up();

  // If we want to seed everytime we start-up the app, uncomment below line
  // await DI.seeder.seed(UserSeeder);

  app.use(cors());
  app.use((req, res, next) => RequestContext.create(DI.orm.em, next));
  app.use(userRouter);
  app.use(errorHandler);
  app.use((req, res) => res.status(404).json({ message: "No route found" }));

  DI.server = app.listen(port, () => {
    console.log(`Povio Assignment (User API) listening at http://localhost:${port}`);
  });
})();
